# Locales

Here you will find all available locales for anything, and that will be served in the server directly without modifications under the path `/locales`.

See [i18next](https://www.i18next.com/translation-function/essentials) documentation about how to translate and how is the process to reference in code that translations. They are really easy to understand.
